class Employee {
    constructor(firstname, lastname, socialSecurityNumber) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.socialSecurityNumber = socialSecurityNumber;
    }
    printE() {
        console.log(this.firstname + ' ' + this.lastname + ' ' + this.socialSecurityNumber);
    }
}

export default Employee;