import Employee from "./Employee.js";

class HourtyEmployee extends Employee {
    constructor(firstname, lastname, socialSecurityNumber, wage, hours) {
        super(firstname, lastname, socialSecurityNumber);
        this.wage = wage;
        this.hours = hours;
    }
    printHE() {
        console.log(this.wage + ' ' + this.hours);
    }
}

export default HourtyEmployee;