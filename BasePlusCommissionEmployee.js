import CommissionEmployee from "./CommissionEmployee.js";


class BasePlus extends CommissionEmployee {
    constructor(firstname, lastname, socialSecurityNumber, grossSales, CommissionRate, baseSalary) {
        super(firstname, lastname, socialSecurityNumber, grossSales, CommissionRate);
        this.baseSalary = baseSalary;
    }
    printBPCE() {
        console.log(this.baseSalary);
    }
}

export default BasePlus;