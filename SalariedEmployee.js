import Employee from "./Employee.js";

class SalariedEmployee extends Employee {
    constructor(firstname, lastname, socialSecurityNumber, weeklySalary) {
        super(firstname, lastname, socialSecurityNumber);
        this.weeklySalary = weeklySalary;
    }
    printSE() {
        console.log(this.weeklySalary);
    }
}

export default SalariedEmployee;