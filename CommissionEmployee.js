import Employee from "./Employee.js";

class CommissionEmployee extends Employee {
    constructor(firstname, lastname, socialSecurityNumber, grossSales, CommissionRate) {
        super(firstname, lastname, socialSecurityNumber);
        this.grossSales = grossSales;
        this.CommissionRate = CommissionRate;
    }
    printCE() {
        console.log(this.grossSales + ' ' + this.CommissionRate);
    }
}

export default CommissionEmployee;