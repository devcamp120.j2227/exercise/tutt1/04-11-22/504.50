import Employee from './Employee.js';
import CommissionEmployee from './CommissionEmployee.js';
import HourtyEmployee from './HourtyEmployee.js';
import SalariedEmployee from './SalariedEmployee.js';
import BasePlus from './BasePlusCommissionEmployee.js';


let emp = new Employee("Ha", "Quang Tien", "HQT98");
emp.printE();
console.log(emp instanceof Employee);


let Cemp = new CommissionEmployee("Ha", "Quang Tien", "HQT98", 10.00, 100.00);
Cemp.printE();
Cemp.printCE();
console.log(Cemp instanceof Employee);


let Hemp = new HourtyEmployee("Ha", "Quang Tien", "HQT98", 8.00, 24.00);
Hemp.printE();
Hemp.printHE();
console.log(Hemp instanceof Employee);


let Semp = new SalariedEmployee("Ha", "Quang Tien", "HQT98", 6.00);
Semp.printE();
Semp.printSE();
console.log(Semp instanceof Employee);


let BSCemp = new BasePlus("Ha", "Quang Tien", "HQT98", 10.00, 100.00, 1000.00);
BSCemp.printE();
BSCemp.printCE();
BSCemp.printBPCE();
console.log(BSCemp instanceof Employee);